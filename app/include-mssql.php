<?php

function do_debug($str) {
    if (array_key_exists('debug', $_COOKIE) && $_COOKIE['debug'] === 'true') {
        print_r(
            '<nav class="navbar fixed-bottom navbar-light bg-light">
            <div class="container-fluid">
            <pre>'.$str.'</pre>
            </div>
            </nav>'
        );
    }
}

function db_connect() {
    $connect = sqlsrv_connect('localhost', ['UID' => 'sa', 'PWD' => 'Password1', 'Database' => 'Produits']);
    if (!$connect) {
        die('Cannot connect to MS SQL');
    }
    return $connect;
}

function get_products($numproduct, $filter) {
    $conn = db_connect();
    $qry = "SELECT * FROM products WHERE (name LIKE '%$filter%' OR description LIKE '%$filter%') AND product_id = $numproduct";
    do_debug($qry);
    $rs = sqlsrv_query($conn, $qry);

    $arr = [];

    while ($row = sqlsrv_fetch_array($rs, SQLSRV_FETCH_ASSOC)) {
        array_push($arr, $row);
    }

    return $arr;
}

function get_all_products() {
    $conn = db_connect();
    $qry = "SELECT * FROM products";
    do_debug($qry);
    $rs = sqlsrv_query($conn, $qry);

    $arr = [];

    while ($row = sqlsrv_fetch_array($rs, SQLSRV_FETCH_ASSOC)) {
        array_push($arr, $row);
    }

    return $arr;
}