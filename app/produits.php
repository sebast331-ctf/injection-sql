<?php include_once 'include-base.php'; ?>
<?php

  if (array_key_exists('filter', $_GET)) {
    $products = get_products($_GET['numproduct'], $_GET['filter']);
  } else {
    $products = get_all_products();
  }

?>

<!doctype html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="">
  <meta name="author" content="Mark Otto, Jacob Thornton, and Bootstrap contributors">
  <meta name="generator" content="Hugo 0.83.1">
  <title>Starter Template · Bootstrap v5.0</title>

  <link rel="canonical" href="https://getbootstrap.com/docs/5.0/examples/starter-template/">


  <!-- CSS only -->
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet"
    integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">

  <style>
    .bd-placeholder-img {
      font-size: 1.125rem;
      text-anchor: middle;
      -webkit-user-select: none;
      -moz-user-select: none;
      user-select: none;
    }

    @media (min-width: 768px) {
      .bd-placeholder-img-lg {
        font-size: 3.5rem;
      }
    }
  </style>


  <!-- Custom styles for this template -->
  <link href="starter-template.css" rel="stylesheet">
</head>

<body>

  <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
    <div class="container-fluid">
      <a class="navbar-brand" href="#">Navbar</a>
      <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav"
        aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarNav">
        <ul class="navbar-nav">
          <li class="nav-item">
            <a class="nav-link " aria-current="page" href="index.php">Accueil</a>
          </li>
          <li class="nav-item">
            <a class="nav-link active" href="produits.php">Produits</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="connexion.php">Connexion</a>
          </li>
        </ul>
      </div>
        <!-- <form class="d-flex" method="get">
          <select name="database" class="form-select" onchange="submit();">
            <option value="mysql" <?php echo get_database() == 'mysql' ? 'selected' : ''; ?>>mysql / mariadb</option>
            <option value="mssql" <?php echo get_database() == 'mssql' ? 'selected' : ''; ?>>Microsoft SQL</option>
          </select>
        </form> -->
    </div>
  </nav>

  <div class="col-lg-8 mx-auto p-3 py-md-5">
    <main>
      
      <h1>Produits</h1>

      <form class="row g-3">
        <div class="col-auto">
          <input name="numproduct" type="text" class="form-control" id="filter" placeholder="No Produit">
        </div>
        <div class="col-auto">
          <input name="filter" type="text" class="form-control" id="filter" placeholder="Recherche">
        </div>
        <div class="col-auto">
          <button type="submit" class="btn btn-primary mb-3">Rechercher</button>
          <a href="produits.php" class="btn btn-primary mb-3" role="button">Tous les produits</a>
        </div>
      </form>

      <table class="table">
        <thread>
          <tr>
            <th scope="col">ID</th>
            <th scope="col">Produit</th>
            <th scope="col">Description</th>
            <th scope="col">Prix</th>
          </tr>
        </thread>
        <tbody>
          <?php foreach ($products as $row) { ?>
            <tr>
              <th scope="row"><?php echo $row['product_id']; ?></th>
              <td><?php echo $row['name']; ?></td>
              <td><?php echo $row['description']; ?></td>
              <td><?php echo $row['price']; ?> $</td>
            </tr>
          <?php } ?>
        </tbody>
      </table>

    </main>
    <footer class="pt-5 my-5 text-muted border-top">
      Created by the Bootstrap team &middot; &copy; 2021
    </footer>
  </div>


  <script src="../assets/dist/js/bootstrap.bundle.min.js"></script>


</body>

</html>