<?php include_once 'include-base.php'; ?>
<?php

  // Login
  $success = null;
  if (array_key_exists('username', $_POST)) {
    $success = login1($_POST['username'], $_POST['password']);

    if ($success) {
      $_SESSION['username'] = $_POST['username'];
      $_SESSION['loggedin'] = true;
    } else {
      session_destroy();
    }
  }
?>

<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="Mark Otto, Jacob Thornton, and Bootstrap contributors">
    <meta name="generator" content="Hugo 0.83.1">
    <title>Signin Template · Bootstrap v5.0</title>

    <link rel="canonical" href="https://getbootstrap.com/docs/5.0/examples/sign-in/">

    <!-- Bootstrap core CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">

    <style>
      .bd-placeholder-img {
        font-size: 1.125rem;
        text-anchor: middle;
        -webkit-user-select: none;
        -moz-user-select: none;
        user-select: none;
      }

      @media (min-width: 768px) {
        .bd-placeholder-img-lg {
          font-size: 3.5rem;
        }
      }
    </style>

    
    <!-- Custom styles for this template -->
    <link href="signin.css" rel="stylesheet">
  </head>
  <body class="text-center">

  
  <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
    <div class="container-fluid">
      <a class="navbar-brand" href="#">Navbar</a>
      <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav"
        aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarNav">
        <ul class="navbar-nav">
          <li class="nav-item">
            <a class="nav-link " aria-current="page" href="index.php">Accueil</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="produits.php">Produits</a>
          </li>
          <li class="nav-item">
            <a class="nav-link active" href="connexion.php">Connexion</a>
          </li>
        </ul>
      </div>
    </div>
  </nav>
    
<main class="form-signin">
  <form method="POST">
    <h1 class="h3 mb-3 fw-normal">Veuillez vous connecter.</h1>

    <?php if ($success) { ?>
      <div class="alert alert-success" role="alert">
        Bonjour <?php echo $_POST['username']; ?>
      </div>
    <?php } elseif ($success !== true) { ?>
      <div class="alert alert-danger" role="alert">  
        <?php echo ($success === false ? 'Usager inconnu' : 'Mauvais mot de passe'); ?>
      </div>
    <?php } ?>

    <div class="form-floating">
      <input name="username" type="text" class="form-control" id="floatingInput" placeholder="Nom d'usager">
      <label for="floatingInput">Nom d'usager</label>
    </div>
    <div class="form-floating">
      <input name="password" type="password" class="form-control" id="floatingPassword" placeholder="Mot de passe">
      <label for="floatingPassword">Mot de passe</label>
    </div>
    <button class="w-100 btn btn-lg btn-primary" type="submit">Connexion</button>
    <p class="mt-5 mb-3 text-muted">&copy; 2017–2021</p>
  </form>
</main>
    
  </body>
</html>
